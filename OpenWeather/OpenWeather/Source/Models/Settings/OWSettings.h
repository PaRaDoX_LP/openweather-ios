//
//  OWSettings.h
//  OpenWeather
//
//  Created by Вячеслав on 19.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SETTINGSMANAGER [OWSettings sharedManager]

@interface OWSettings : NSObject

+ (id)sharedManager;

- (void)initializeWithMainController;
- (void)setupOpenWeatherAPI;

@end
