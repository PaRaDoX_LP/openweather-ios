//
//  YWSettings.m
//  OpenWeather
//
//  Created by Вячеслав on 19.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OWSettings.h"
#import "OWMainViewController.h"

@interface OWSettings()

@property(nonatomic, strong, readonly) UINavigationController *navigationController;
@property(nonatomic, strong, readonly) UIWindow *keyWindow;

@end

@implementation OWSettings

+ (id)sharedManager {
    static OWSettings *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[OWSettings alloc] init];
    });
    return _sharedManager;
}

- (void) initializeWithMainController
{
    //init window
    _keyWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    //init navigation
    _navigationController = [[UINavigationController alloc] initWithNavigationBarClass:nil toolbarClass:nil];
    _keyWindow.rootViewController = _navigationController;
    [_keyWindow makeKeyAndVisible];
    [self setupNavBarAppearenceForNavController];
    
    OWMainViewController *viewController = [OWMainViewController viewControllerFromDefaultNib];
    [_navigationController pushViewController:viewController animated:YES];
}

- (void) setupOpenWeatherAPI
{
}

#pragma mark - UI Customization

- (void)setupNavBarAppearenceForNavController {
    
    //Make navbar transparent
    [_navigationController.navigationBar setBackgroundImage:[UIImage new]
                                              forBarMetrics:UIBarMetricsDefault];
    _navigationController.navigationBar.shadowImage = [UIImage new];
    _navigationController.navigationBar.translucent = YES;
    _navigationController.view.backgroundColor = [UIColor clearColor];
    _navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [_navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor yellowColor]}];
    [_navigationController.navigationBar setTintColor:[UIColor yellowColor]];
}

@end
