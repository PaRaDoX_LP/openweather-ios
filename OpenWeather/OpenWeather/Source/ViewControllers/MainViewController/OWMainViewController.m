//
//  YWMainViewController.m
//  OpenWeather
//
//  Created by Вячеслав on 19.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "OWMainViewController.h"
#import "OWAddCityViewController.h"

@interface OWMainViewController ()

@end

@implementation OWMainViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"OpenWeather"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bar_btn_add_location"] style:UIBarButtonItemStylePlain target:self action:@selector(addNewCity)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Events
#pragma mark - Actions

- (void) addNewCity
{
    OWAddCityViewController *viewController = [OWAddCityViewController viewControllerFromDefaultNib];
    [self.navigationController pushViewController:viewController animated:TRUE];
}

@end
