//
//  YWAddCityViewController.m
//  OpenWeather
//
//  Created by Вячеслав on 21.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "OWAddCityViewController.h"

@interface OWAddCityViewController ()

@end

@implementation OWAddCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //add back button
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"]
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(backButtonClicked)];
    back.title = @"";
    self.navigationItem.leftBarButtonItem = back;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Events

- (void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
