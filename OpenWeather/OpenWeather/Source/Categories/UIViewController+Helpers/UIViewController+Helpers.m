//
//  UIViewController+Helpers.m
//  OpenWeather
//
//  Created by Вячеслав on 19.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "UIViewController+Helpers.h"

@implementation UIViewController (Helpers)

#pragma mark - Implementation helpers

+ (instancetype)viewControllerFromDefaultNib {
    id vc = [[[self class] alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
    return vc;
}

@end
