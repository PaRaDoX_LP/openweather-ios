//
//  UIViewController+Helpers.h
//  OpenWeather
//
//  Created by Вячеслав on 19.09.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Helpers)

+ (instancetype) viewControllerFromDefaultNib;

@end
